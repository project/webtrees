<?php

namespace Drupal\webtrees_views\Helper;

/**
 * Provides Webtrees database access.
 *
 */
class FavoritesDatabase {
  /**
   * The Webtrees table prefix.
   *
   * @var string
   */
  public $webtreesPrefix;

  /**
   * The Webtrees URL prefix.
   *
   * @var string
   */
  public $webtreesUrl;

  /**
   * Drupal database object.
   *
   * @var Database $database
   */
  public $database;

  /**
   * Webtrees trees.
   *
   * @var array $trees
   */
  public $tree = [];

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $config = \Drupal::config('webtrees.settings');
    $this->webtreesPrefix = $config->get('database.prefix');
    $this->webtreesUrl = $config->get('configuration.url');
    $this->database = \Drupal::database();

    foreach($this->getRecords(['table' => 'gedcom']) as $tree_record) {
      $this->tree[$tree_record->gedcom_id] = $tree_record->gedcom_name;
    }
  }

  /**
   * Get Tree name.
   *
   * @param integer $tree_id
   *
   * @return string
   */
  public function getTreeName($tree_id) {
    return isset($this->tree[$tree_id]) ? $this->tree[$tree_id] : '';
  }

  /**
   * Get Webtrees table name.
   *
   * @param string $table
   * 
   * @return string
   */
  public function webtreesTable($table) {
    return $this->webtreesPrefix . $table;
  }

  /**
   * Get Webtrees settings for user.
   *
   * @param int $user_id
   *
   * @result array
   */
  public function getSettings($user_id) {
    $result = $this->getRecordField([
      'table' => 'user_setting',
      'condition' => [
        'user_id' => $user_id,
        'setting_name' => 'favorites-menu-settings',
        ],
      'field' => 'setting_value',
      ]);

    if ($result) {
      return unserialize($result);
    }
    return ['default_group' => ''];
  }

  /**
   * Get Webtrees shared groups for user.
   *
   * @param int $user_id
   *
   * @result array
   */
  public function getSharedGroups($user_id) {
    $result = $this->getRecordField([
      'table' => 'user_setting',
      'condition' => [
        'user_id' => $user_id,
        'setting_name' => 'favorites-menu-shared',
        ],
      'field' => 'setting_value',
      ]);

    if ($result) {
      return unserialize($result);
    }
    return [];
  }

  /**
   * Get Webtrees user.
   *
   * @param int $user_id
   *
   * @result array
   */
  public function getUser($user_id) {
    $result = $this->getRecords([
      'table' => 'user',
      'condition' => [
        'user_id' => $user_id,
        ],
      ]);

    return $result ? $result[0] : [];
  }

  /**
   * Fetch Webtrees GEDCOM records.
   *
   * @param array $param
   *   Includes [table] and optional [condition].
   *
   * @return array
   *   Row object elements.
   */
  public function getRecords($param) {
    $query = $this->database
      ->select($this->webtreesTable($param['table']),'table')
      ->fields('table');
    if (isset($param['condition'])) {
      foreach($param['condition'] as $field => $value) {
        if (is_array($value)) {
          $query->condition($field, $value[0], $value[1]);
        } else {
          $query->condition($field, $value);
        }
      }
    }

    if (isset($param['orderBy'])) {
      foreach($param['orderBy'] as $field => $value) {
        $query->orderBy($field, $value);
      }
    }

    if (isset($param['limit'])) {
      [$field, $value] = $param['limit'];
      $query->range($field, $value);
    }

    return $query
      ->execute()
      ->fetchAll();
  }

  /**
   * Fetch field from a single GEDCOM record.
   *
   * @param array $param
   *   Requires $param['field'].
   *   Compatible with getRecords.
   *
   * @return string
   */
  public function getRecordField($param) {
    $result = $this->getRecords($param);
    $field = $param['field'];
    return
      isset($result[0]->$field) ?
      $result[0]->$field :
      '';
  }

  /**
   * Fetch GEDCOM record full name.
   *
   * Assumes a singel matching record.
   * Patterns $param can be used to extract data. 
   *
   * @param array $param
   *   Compatible with getRecordField
   *
   * @return string
   */
  public function getFullName($param) {
    $result = $this->getRecordField($param);

    if ($result) { 
      if (isset($param['pattern'])) {
        $matches = [];
        foreach($param['pattern'] as $pattern) {
          if (preg_match($pattern,$result,$matches)) {
            // Strip full match result.
            unset($matches[0]);
            return implode(' ',$matches);
          }
        }
        return '';
      }
      return $result;
    }

    return '';
  }


  /**
   * Get the full name of an individual.
   *
   * @param string $tree_id
   *
   * @param string $gedcom_id
   *
   * @return string
   */
  public function getIndividualFullName($tree_id, $gedcom_id) {
    return $this->getFullName([
          'table' => 'individuals',
          'condition' => [
            'i_file' => $tree_id,
            'i_id' => $gedcom_id,
            ],
          'field' => 'i_gedcom',
          'pattern' => ['#NAME (.*) /(.*)/#'],
          ]);    
  }

  /**
   * Load GEDCOM record.
   *
   * @param string $type
   *   May be: INDI, FAM, NOTE.
   *
   * @param string $tree_id
   *
   * @param string $gedcom_id
   *
   * @return object
   */
  public function loadGedcom($type, $tree_id, $gedcom_id) {
    switch ($type) {
      case 'INDI':
        $fullName = $this->getIndividualFullName($tree_id, $gedcom_id);
        $path = 'individual';
        break;
      case 'FAM':
        $path = 'family';
        $records = $this->getRecords([
          'table' => 'families',
          'condition' => [
            'f_id' => $gedcom_id,
            'f_file' => $tree_id,
            ],
          ]);

        $fullName =
          $records ?
            $this->getIndividualFullName($tree_id, $records[0]->f_husb) .
            ' + ' .
            $this->getIndividualFullName($tree_id, $records[0]->f_wife) :
          '';
        break;
      case 'OBJE':
        $path = 'media';
        $fullName = $this->getFullName([
          'table' => 'media',
          'condition' => [
            'm_id' => $gedcom_id,
            'm_file' => $tree_id,
            ],
          'field' => 'm_gedcom',
          'pattern' => ['#TITL (.*)#', '#FILE (.*)#'],
          ]);
        break;
      case 'NOTE':
        $path = 'note';
        $fullName = $this->getFullName([
          'table' => 'other',
          'condition' => [
            'o_id' => $gedcom_id,
            'o_file' => $tree_id,
            'o_type' => 'NOTE',
            ],
          'field' => 'o_gedcom',
          'pattern' => ['#NOTE (.*)#'],
          ]);
        break;
      case 'REPO':
        $path = 'repository';
        $fullName = $this->getFullName([
          'table' => 'other',
          'condition' => [
            'o_id' => $gedcom_id,
            'o_file' => $tree_id,
            'o_type' => 'REPO',
            ],
          'field' => 'o_gedcom',
          'pattern' => ['#NAME (.*)#', '#REPO (.*)#'],
          ]);
        break;
      case 'SOUR':
        $path = 'source';
        $fullName = $this->getFullName([
          'table' => 'sources',
          'condition' => [
            's_id' => $gedcom_id,
            's_file' => $tree_id,
            ],
          'field' => 's_gedcom',
          'pattern' => ['#TITL (.*)#', '#SOUR (.*)#'],
          ]);
        break;
      default:
        return null;
    }

    $tree_name = $this->getTreeName($tree_id);
    return (object) [
        'type' => $type,
        'tree_id' => $tree_id,
        'gedcom_id' => $gedcom_id,
        'fullName' => $fullName,
        'url' => $this->webtreesUrl . "tree/$tree_name/$path/$gedcom_id" ,
        ];
  }
}
