<?php

namespace Drupal\webtrees_views\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;

/**
 * Render a Webtrees family GEDCOM field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("webtrees_family")
 */
class WebtreesFamily extends WebtreesFieldBase {
  /**
   * Webtress type used in path.
   *
   * @var string
   */
  public $webtreesType = 'family';

  /**
   * Webtress index field.
   *
   * @var string
   */
  public $webtreesId = 'f_id';

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['gedcom_format'] = ['default' => 'id'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['gedcom_format'] = [
      '#title' => $this->t('GEDCOM Format'),
      '#options' => [
        'raw' => t('Raw'),
        'id' => t('ID'),
        'link' => t('Link'),
      ],
      '#type' => 'select',
      '#default_value' => $this->options['gedcom_format'],
      '#description' => $this->t('Choose how the GEDCOM record is presented.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $value = $this->getValue($values);
    $format = $this->options['gedcom_format'];

    switch ($format) {
      case 'id':
        if (preg_match('"0 @(.*)@ FAM"', $value, $matches)) {
          $value = $matches[1];
        }
        else {
          $value = '';
        }
        break;

      case 'link':
        $value = $this->webtreesUrl;
        break;
    }

    return $value;
  }

}
