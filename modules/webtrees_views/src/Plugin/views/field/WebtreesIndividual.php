<?php

namespace Drupal\webtrees_views\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;

/**
 * Render a Webtrees invidual GEDCOM field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("webtrees_individual")
 */
class WebtreesIndividual extends WebtreesFieldBase {
  /**
   * Webtress type used in path.
   *
   * @var string
   */
  public $webtreesType = 'individual';

  /**
   * Webtress index field.
   *
   * @var string
   */
  public $webtreesId = 'i_id';

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['gedcom_format'] = ['default' => 'name'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['gedcom_format'] = [
      '#title' => $this->t('GEDCOM Format'),
      '#options' => [
        'raw' => t('Raw'),
        'name' => t('Name'),
        'first' => t('First name'),
        'last' => t('Last name'),
        'id' => t('ID'),
        'link' => t('Link'),
      ],
      '#type' => 'select',
      '#default_value' => $this->options['gedcom_format'],
      '#description' => $this->t('Choose how the GEDCOM record is presented.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $value = $this->getValue($values);
    $format = $this->options['gedcom_format'];

    switch ($format) {
      case 'id':
        if (preg_match('"0 @(.*)@ INDI"', $value, $matches)) {
          $value = $matches[1];
        }
        else {
          $value = '';
        }
        break;

      case 'link';
        $value = $this->webtree_url;
        break;

      case 'name':
      case 'first':
      case 'last';
        // Looking for "1 NAME William George /Wong/".
        // Matches include full string and each element.
        $matches = [];
        $first = '';
        $last = '';
        preg_match('"1 NAME (.*) /(.*)/"', $value, $matches);
        switch (count($matches)) {
          case 0:
            return '';

          case 2:
            $first = $matches[1];
            break;

          default:
            $first = $matches[1];
            $last  = $matches[2];
            break;
        }

        switch ($format) {
          case 'first':
            $value = $first;
          case 'last';
            $value = $last;
          default:
            $value = "$first $last";
        }
        break;
    }

    return $value;
  }

}
