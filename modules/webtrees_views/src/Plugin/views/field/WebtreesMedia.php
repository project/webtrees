<?php

namespace Drupal\webtrees_views\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\GeneratedLink;

/**
 * Render a Webtrees family GEDCOM field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("webtrees_media")
 */
class WebtreesMedia extends WebtreesFieldBase {
  /**
   * Webtress type used in path.
   *
   * @var string
   */
  public $webtreesType = 'media';

  /**
   * Webtress index field.
   *
   * @var string
   */
  public $webtreesId = 'm_id';

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['gedcom_format'] = ['default' => 'raw'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['gedcom_format'] = [
      '#title' => $this->t('Media Format'),
      '#options' => [
        'raw' => t('File name'),
        'image' => t('Image'),
        'id' => t('ID'),
        'link' => t('Link'),
      ],
      '#type' => 'select',
      '#default_value' => $this->options['gedcom_format'],
      '#description' => $this->t('Choose how the media record is presented.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function addAdditionalFields($fields = NULL) {
    if ($this->options['gedcom_format'] == 'image') {
      $this->additional_fields['media_gedcom'] = [
        'table' => webtreesPrefix() . 'media',
        'field' => 'm_gedcom',
      ];
    }

    return parent::addAdditionalFields($fields);
  }

  /**
   * Generate Webtrees media URL.
   *
   * @param array $values
   *   List of query results.
   *
   * @return string
   *   URL for a Webtrees media object.
   */
  protected function webtreesGetUrl($values) {
    $config = \Drupal::config('webtrees.settings');
    $prefix = $config->get('configuration.url');
    $id = $this->getValue($values, $this->webtreesId);
    $tree = $this->getValue($values, 'gedcom_name');

    // Extract basic GEDCOM information only.
    // Assumes a well formed GEDCOM record.
    $lines = preg_split(
      "/\\r\\n|\\r|\\n/",
      $this->getValue($values,
      'media_gedcom'));
    $gedcom = [];
    $found = FALSE;
    foreach ($lines as $line) {
      $code = substr($line, 0, 2);
      if ('0 ' == $code) {
        continue;
      }
      elseif ('1 ' == $code) {
        if ($found) {
          break;
        }
        $found = 1;
      }
      $gedcom[] = $line;
    }

    $fact_id = md5($i = implode("\n", $gedcom));

    return $prefix . 'index.php?route=' . $prefix . "tree/$tree/media-download&disposition=inline&xref=$id&fact_id=$fact_id";
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $value = $this->getValue($values);
    $format = $this->options['gedcom_format'];

    switch ($format) {
      case 'id':
        if (preg_match('"0 @(.*)@ FAM"', $value, $matches)) {
          $value = $matches[1];
        }
        else {
          $value = '';
        }
        break;

      case 'link':
        $value = $this->webtreesGetUrl($values);
        break;

      case 'image':
        $path = $this->webtreesGetUrl($values);
        $generated_link = new GeneratedLink();
        $generated_link->setGeneratedLink($x = "<img src='$path' class='webtrees-image' />");
        return $generated_link;
    }

    return $value;
  }

}
