<?php

namespace Drupal\webtrees_views\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\webtrees_views\Helper\FavoritesDatabase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "favorites_block",
 *   admin_label = @Translation("Favorites block"),
 *   category = @Translation("Webtrees"),
 * )
 */
class FavoritesBlock extends BlockBase {
  /**
   * Drupal database object.
   *
   * @var Drupal\webtrees_views\Helper\FavoritesDatabase
   */
  protected $webtrees;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->webtrees = new FavoritesDatabase;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // Force update. Allows changes to be made in another tab/window.
    return 0;
  }


  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $settings = parent::defaultConfiguration();
    $setting['showSecondary'] = TRUE;
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    return [
      'showSecondary' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Show secondary menus'),
        '#default_value' => $this->configuration['showSecondary'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['showSecondary'] = $form_state->getValue('showSecondary');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $user_id = 1;
    $settings = $this->webtrees->getSettings($user_id);
    $default_group = $settings['default_group'];

    // Get tree name and id.
    $last_session = $this->webtrees->getRecordField([
      'table' => 'session',
      'field' => 'session_data',
      'condition' => [
        'user_id' => $user_id,
        'session_data' => ['%last_gedcom_id%', 'LIKE'],
        ],
      'orderBy' => [ 'session_time' => 'DESC' ],
      'limit' => [0, 1],
      ]);
      
    if ( (!$last_session)
      || (!preg_match('/last_gedcom_id\|i\:([0-9]*)\;/',$last_session,$match))
      || (!isset($this->webtrees->tree[$match[1]]))) {
      // Show notihing if we cannot find the tree.
      return [];
    }

    // Add primary menu.
    $tree_id = $match[1];
    $tree_name = $this->webtrees->getTreeName($tree_id);
    $url_prefix = $this->webtrees->webtreesUrl;
    $submenu[] = [
      'title' => $default_group,
      'url' =>$url_prefix."tree/$tree_name/favorites-menu",
      'class' => 'favorites-menu-first-group favorites-menu-group favorites-menu-item'
      ];

    $this->getSubmenu($default_group, $submenu, $user_id, $tree_id);

    // Add secondary menus.
    if ($this->configuration['showSecondary']) {
      foreach($settings['secondary'] as $value) {
        // Do not add if no secondary menu selected.
        if (preg_match('#(.*)\,(.*)#', $value,$matches)) {
          $group_user_id = $matches[1];
          $group = $matches[2];

          if ($group_user_id) {
            $group_user = $this->webtrees->getUser($group_user_id);
            if (in_array($group,$this->webtrees->getSharedGroups($group_user_id))) {
              // Include separator.
              $submenu[] = [];
              $submenu[] = [
                 'title' => $group .
                   ($group_user_id != $user_id ?
                    ' @ ' . $group_user->real_name :
                    ''),
                 'url' => $url_prefix."tree/$tree_name/favorites-menu",
                 'class' => "favorites-menu-group favorites-menu-item",
                 ];;
              $this->getSubmenu($group, $submenu, $group_user_id, $tree_id);
            }
          }
        }
      }
    }

    return [
      '#theme' => 'webtrees_favorites_block',
      '#data' => $submenu,
    ];
  }

  /**
   * Get submenu.
   *
   * @param string  $group
   *
   * @param array   &$submenu
   *
   * @param integer $user_id
   *
   * @param integer $tree_id
   *   Used to check for items in another tree.
   */
   private function getSubmenu($group, &$submenu, $user_id, $tree_id) {
    $favorites = $this->getFavorites($user_id);
    foreach(['INDI', 'FAM', 'OBJE', 'URL', 'NOTE', 'SOUR', 'REPO' ] as $type) {
      foreach($favorites as $favorite) {
        if (($type == $favorite['type']) && ($group == $favorite['group'])) {
          $submenu[] = [
            'title' => $favorite['title'],
            'url' => $favorite['url'],
            'class' => "favorites-menu-$type favorites-menu-item" .
              ($favorite['gedcom_id'] && ($favorite['gedcom_id'] != $tree_id) ?
               ' favorites-menu-other-tree' :
               '')
            ];
        }
      }
    }
  }

  /**
   * Get all favorites.
   *
   * @param integer $user_id
   *
   * @return array
   *   Array of arrays with keys: group, title, type, url.
   */
  public function getFavorites($user_id):array {
    $result = [];
    $favorites = $this->webtrees->getRecords([
      'table' => 'favorite',
      'condition' => [ 'user_id' => $user_id],
      ]);

    foreach ($favorites as $favorite) {
      $ftype = $favorite->favorite_type;
      if ($ftype == 'URL') {
        $result[$favorite->favorite_id] = [
          'favorite_id' => $favorite->favorite_id,
          'title' => htmlspecialchars($favorite->title),
          'type' => 'URL',
          'url' => $favorite->url,
          'group' => $favorite->note ? $favorite->note : '',
          'xref' => '',
          'gedcom_id' => '',
        ];
      } elseif ($favorite->xref) {
        $record = $this->webtrees->loadGedcom($ftype, $favorite->gedcom_id, $favorite->xref);
        $result[$favorite->favorite_id] = [
            'favorite_id' => $favorite->favorite_id,
            'title' => htmlspecialchars($record->fullName),
            'type' => $ftype,
            'url' => htmlspecialchars($record->url),
            'group' => $favorite->note ? $favorite->note : '',
            'xref' => $favorite->xref,
            'gedcom_id' => $favorite->gedcom_id,
            ];
      }
    }
    return $result;
  }
}

