<?php

namespace Drupal\webtrees_views\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\GeneratedLink;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;

/**
 * Render a Webtrees base GEDCOM field.
 *
 * @ingroup views_field_handlers
 */
class WebtreesFieldBase extends FieldPluginBase {
  /**
   * Webtress type used in path.
   *
   * @var string
   */
  public $webtreesType = '';

  /**
   * Webtress index field.
   *
   * @var string
   */
  public $webtreesId = '';

  /**
   * Webtress field URL.
   *
   * @var string
   */
  public $webtreesUrl = '';

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['gedcom_format'] = ['default' => 'id'];
    $options['gedcom_add_link'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    $id = $this->webtreesId;
    $this->additional_fields[$id] = $id;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['gedcom_add_link'] = [
      '#title' => $this->t('Link to the content'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['gedcom_add_link'],
      '#description' => $this->t('Link to the individual record page.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function addAdditionalFields($fields = NULL) {
    if ($this->options['gedcom_add_link']
       || ($this->options['gedcom_format'] == 'link')
       || ($this->options['gedcom_format'] == 'image')
    ) {
      $this->additional_fields['gedcom_name'] = [
        'table' => webtreesPrefix() . 'gedcom',
        'field' => 'gedcom_name',
      ];
    }

    return parent::addAdditionalFields($fields);
  }

  /**
   * Generate Webtrees media URL.
   *
   * @param array $values
   *   A list of query results.
   *
   * @return string
   *   URL for Webtrees item.
   */
  protected function webtreesGetUrl($values) {
    $config = \Drupal::config('webtrees.settings');
    $prefix = $config->get('configuration.url');
    $id = $this->getValue($values, $this->webtreesId);
    $tree = $this->getValue($values, 'gedcom_name');
    $type = $this->webtreesType;
    $path = $prefix . "tree/$tree/$type/$id";
    return $prefix . 'index.php?route=' . $path;
  }

  /**
   * {@inheritdoc}
   */
  public function advancedRender(ResultRow $values) {
    // Generate link if needed and $values available.
    if ($this->options['gedcom_add_link']
       || ($this->options['gedcom_format'] == 'link')
       || ($this->options['gedcom_format'] == 'image')
    ) {
      $this->webtreesUrl = $this->webtreesGetUrl($values);
    }

    return parent::advancedRender($values);
  }

  /**
   * {@inheritdoc}
   */
  public function renderText($alter) {
    $value = parent::renderText($alter);

    if (empty($alter['make_link'])
       && isset($this->options['gedcom_add_link'])
       && $this->options['gedcom_add_link']) {
      $url = $this->webtreesUrl;
      $generated_link = new GeneratedLink();
      $generated_link->setGeneratedLink($x = "<a href='$url' class='webtrees-link'>$value</a>");

      return $generated_link;
    }
    return $value;
  }

}
