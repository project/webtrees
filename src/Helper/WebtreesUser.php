<?php

namespace Drupal\webtrees\Helper;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormStateInterface;

/**
 * Manage interaction with Webtrees user database.
 */
class WebtreesUser {
  /**
   * Webtrees database.
   *
   * @var \mysqli
   */
  public $database;

  /**
   * Webtrees module settings.
   *
   * @var \Drupal\Core\Config\ConfigBase
   */
  public $myConfig;

  /**
   * Webtrees user ID.
   *
   * @var int
   */
  public $userId;

  /**
   * Webtrees user name.
   *
   * @var string
   */
  public $userName;

  /**
   * Webtrees user's real name.
   *
   * @var string
   */
  public $realName;

  /**
   * Webtrees user password.
   *
   * @var string
   */
  public $password;

  /**
   * Webtrees user password hash.
   *
   * @var string
   */
  public $hash;

  /**
   * Webtrees user email.
   *
   * @var string
   */
  public $email;

  /**
   * Webtrees role.
   *
   * @var string
   */
  public $role;

  /**
   * Validate error text. Used by validateLoging.
   *
   * @var string
   */
  protected $errorMessage;

  /**
   * Login name. Used by validateLoging.
   *
   * @var string
   */
  protected $loginName;

  /**
   * Login password. Used by validateLoging.
   *
   * @var string
   */
  protected $loginPassword;

  /**
   * Login valid flag.
   *
   * @var bool
   */
  protected $loginValid;

  /**
   * Webtrees settings.
   *
   * @var array
   */
  public $settings;

  /**
   * Constructs a WebtreesUser object.
   */
  public function __construct() {
    $myConfig = \Drupal::config('webtrees.settings');
    if ($myConfig->get('database.use_drupal')) {
      $databases = Database::getConnectionInfo('default');
      $host = $databases['default']['host'];
      $user = $databases['default']['username'];
      $password = $databases['default']['password'];
      $port = $databases['default']['port'];
    }
    else {
      $host = $myConfig->get('database.host');
      $user = $myConfig->get('database.user');
      $password = $myConfig->get('database.password');
      $port = $myConfig->get('database.port');
    }

    // Always use our database and prefix.
    $database = $myConfig->get('database.database');

    $this->database = new \mysqli($host, $user, $password, $database, $port);

    $this->myConfig = $myConfig;
    // Initially invalid user.
    $this->userId = 0;
  }

  /**
   * Check for database error.
   *
   * @return string
   *   Database connect error.
   */
  public function connectError() {
    return $this->database->connect_error;
  }

  /**
   * Check if current user information is valid.
   *
   * $userId==0 indicates invalid information.
   *
   * @return bool
   *   False indicates invalid user.
   */
  public function isLoaded() {
    return $this->userId;
  }

  /**
   * Load Webtrees user by id, name, email or name/email.
   *
   * @param string $string
   *   SELECT argument.
   * @param string $type
   *   Should be: id, name, email, name_or_email.
   *
   * @return bool
   *   True if load succeeds.
   */
  public function load($string, $type = 'id') {
    // In case fetch fails.
    $this->userId = 0;
    $prefix = $this->myConfig->get('database.prefix');

    switch ($type) {
      case 'name_or_email':
        $stmt = $this->database->prepare('SELECT * FROM ' . $prefix . 'user WHERE user_name=? OR email=?');
        $stmt->bind_param('ss', $string, $string);
        break;

      case 'name':
        $stmt = $this->database->prepare('SELECT * FROM ' . $prefix . 'user WHERE user_name=?');
        $stmt->bind_param('s', $string);
        break;

      case 'email':
        $stmt = $this->database->prepare('SELECT * FROM ' . $prefix . 'user WHERE email=?');
        $stmt->bind_param('s', $string);
        break;

      case 'id':
        $stmt = $this->database->prepare('SELECT * FROM ' . $prefix . 'user WHERE user_id=?');
        $stmt->bind_param('i', $string);
        break;

      default;
        // Fail silently.
        $this->log(t('Unknown type for function load: @type error', ['@type' => $type]));
        return FALSE;
    }

    $stmt->execute();
    $stmt->bind_result($this->userId, $this->userName, $this->realName, $this->email, $this->hash);
    $result = $stmt->fetch();
    $stmt->close();

    if ($this->userId) {
      // Load user settings.
      $stmt = $this->database->prepare('SELECT setting_name,setting_value FROM ' . $prefix . 'user_setting WHERE user_id=?');
      $stmt->bind_param('i', $this->userId);
      $stmt->execute();
      $stmt->bind_result($setting_name, $setting_value);

      $this->settings = [];
      while ($stmt->fetch()) {
        $this->settings[$setting_name] = $setting_value;
      }
      $stmt->close();

      $stmt = $this->database->prepare('SELECT setting_value FROM ' . $prefix . 'user_gedcom_setting WHERE user_id=? and setting_name="canedit"');
      $stmt->bind_param('i', $this->userId);
      $stmt->execute();
      $stmt->bind_result($this->role);
      if (!$stmt->fetch()) {
        // Member.
        $this->role = 'access';
      }

      $stmt->close();
    }

    return $this->isLoaded();
  }

  /**
   * Update or create webtrees user.
   *
   * Update only modifies the main user record that includes the password.
   * Create will use default values.
   */
  public function save() {
    $prefix = $this->myConfig->get('database.prefix');

    if ($this->isLoaded()) {
      // Update user record.
      $stmt = $this->database->prepare('UPDATE ' . $prefix . 'user SET user_name=?,real_name=?,email=?,password=? WHERE user_id=?');
      $stmt->bind_param('ssssi', $this->userName, $this->realName, $this->email, $this->hash, $this->userId);
      $stmt->execute();
      $stmt->close();
    }
    else {
      // Create new user record.
      $stmt = $this->database->prepare('INSERT INTO ' . $prefix . 'user (user_name,real_name,email,password) VALUES (?,?,?,?)');
      $stmt->bind_param('ssss', $this->userName, $this->realName, $this->email, $this->hash);
      $stmt->execute();
      $stmt->close();

      // Get the userId.
      $stmt = $this->database->prepare('SELECT user_id FROM ' . $prefix . 'user WHERE user_name=?');
      $stmt->bind_param('s', $this->userName);
      $stmt->execute();
      $stmt->bind_result($this->userId);
      $result = $stmt->fetch();
      $stmt->close();

      // Create user_gedcom_setting entries.
      $gedcom = $this->myConfig->get('webtrees_user.gedcom');

      $stmt = $this->database->prepare(
            'INSERT INTO ' . $prefix . 'user_gedcom_setting (userId,gedcom_id,setting_name,setting_value)
VALUES (?,?,"canedit",?), (?,?,"gedcom_id","0"),(?,?,"RELATIONSHIP_PATH_LENGTH","0")'
           );
      $stmt->bind_param('issisis', $this->userId, $gedcom, $this->role, $this->userId, $gedcom, $this->userId, $gedcom);
      $stmt->execute();
      $stmt->close();

      // Copy default block settings
      // The default settings have userId == -1.
      $stmt = $this->database->prepare(
            'SELECT gedcom_id,xref,location,block_order,module_name FROM ' . $prefix . 'block WHERE user_id=-1');
      $stmt->execute();
      $stmt->bind_result($gedcom_id, $xref, $location, $block_order, $module_name);
      $rows = [];
      while ($stmt->fetch()) {
        $rows[] = [
          'gedcom_id' => $gedcom_id,
          'xref' => $xref,
          'location' => $location,
          'block_order' => $block_order,
          'module_name' => $module_name,
        ];
      }
      $stmt->close();

      $stmt = $this->database->prepare(
            'INSERT INTO ' . $prefix . 'block (gedcom_id,userId,xref,location,block_order,module_name) VALUES '
           . '(?,?,?,?,?,?)'
           );
      foreach ($rows as $row) {
        $stmt->bind_param(
           'ssssss',
           $row['gedcom_id'],
           $this->userId,
           $row['xref'],
           $row['location'],
           $row['block_order'],
           $row['module_name']
           );
        $stmt->execute();
      }
      $stmt->close();

      // Create user_setting entries.
      $this->saveSettings();
    }
  }

  /**
   * Verify $password_hash matches password.
   *
   * @param string $password
   *   Cleartext password.
   *
   * @return bool
   *   TRUE if paswords match.
   */
  public function passwordVerify($password) {
    $this->password = $password;

    // Webtrees uses different encryption depending upon the version.
    return (PHP_VERSION_ID < 50307 && password_hash('foo', PASSWORD_DEFAULT) === FALSE)
           ? crypt($password, $this->hash) === $this->hash
           : password_verify($password, $this->hash);
  }

  /**
   * Set Webtrees user password.
   *
   * @param string $password
   *   Cleartext password.
   */
  public function setPassword($password) {
    // Webtrees uses different encryption depending upon the version.
    $this->password = $password;
    $this->hash = (PHP_VERSION_ID < 50307 && password_hash('foo', PASSWORD_DEFAULT) === FALSE)
                ? crypt($password)
                : password_hash($password, PASSWORD_DEFAULT);
  }

  /**
   * Check if Webtrees user is enabled.
   *
   * @return bool
   *   TRUE if user enabled.
   */
  public function isBlocked() {
    return !($this->settings['verified'] || $this->settings['verified_by_admin']);
  }

  /**
   * Get setting value.
   *
   * @param string $setting_name
   *   Setting name.
   *
   * @return string
   *   Setting value.
   */
  public function get($setting_name) {
    return $this->settings[$setting_name] ?? '';
  }

  /**
   * Set setting value.
   *
   * @param string $setting_name
   *   Setting name.
   * @param string $setting_value
   *   New value.
   */
  public function set($setting_name, $setting_value) {
    $this->settings[$setting_name] = $setting_value;
  }

  /**
   * Save setting values.
   *
   * Assumes valid $userId
   * Updates or adds settings. It will not remove settings.
   */
  public function saveSettings() {
    $table_name = $this->myConfig->get('database.prefix') . 'user_setting';
    $get = $this->database->prepare('SELECT setting_value FROM ' . $table_name . ' WHERE user_id=? AND setting_name=?');
    $get->bind_param('is', $this->userId, $setting_name);
    $get->bind_result($setting_value);

    $add = $this->database->prepare('INSERT INTO ' . $table_name . ' VALUES (?,?,?)');
    $add->bind_param('iss', $this->userId, $setting_name, $setting_value);

    foreach ($this->settings as $setting_name => $setting_value) {
      $get->execute();
      if ($get->fetch()) {
        $put = $this->database->prepare("UPDATE $table_name SET $setting_name=? WHERE user_id=?");
        $put->bind_param('si', $setting_value, $this->userId);
        $put->execute();
        $put->close();
      }
      else {
        $add->execute();
      }
    }
    $get->close();
    $add->close();
  }

  /**
   * Close database.
   */
  public function close() {
    $this->database->close();
  }

  /**
   * Log message.
   *
   * @param string $message
   *   Translated message.
   * @param string $type
   *   One of: alert, critical, debug, emergency, error, info, notice, warning.
   */
  public function log($message, $type = 'info') {
    switch ($type) {
      case 'info':
        if ($this->myConfig->get('configuration.logging')) {
          \Drupal::logger('webtrees')->log($type, $message);
        }
        break;

      // Case 'debug':
      // Need to add a debug configuration option and debug hooks sometime.
      default:
        \Drupal::logger('webtrees')->log($type, $message);
        break;
    }
  }

  /**
   * Flag error. Used by validateLoging.
   *
   * @param string $message
   *   Untranslated error message.
   */
  public function setError(string $message) {
    $this->errorMessage = $message;
  }

  /**
   * Get error message.
   *
   * @return string
   *   Untranslated error message.
   */
  public function getError() {
    return $this->errorMessage;
  }

  /**
   * See if we are done validating.
   *
   * @return bool
   *   TRUE when done validation.
   */
  protected function doneValidate() {
    return $this->errorMessage || $this->loginValid;
  }

  /**
   * Validate log in form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Login form state to validate.
   */
  public function validateLogin(FormStateInterface $form_state) {
    $this->loginValid = FALSE;

    if ($this->myConfig->get('configuration.enable')) {
      if ($this->connectError()) {
        $this->error('Webtrees login enabled but database is inaccessible');
      }
      else {
        // Load users from both lists.
        $this->loginName = trim($form_state->getValue('name'));
        $this->loginPassword = trim($form_state->getValue('pass'));

        // See what user info to use for logging in.
        if ($this->myConfig->get('configuration.use_webtrees')) {
          $primary = 'webtrees';
          $secondary = 'drupal';
        }
        else {
          $primary = 'drupal';
          $secondary = 'webtrees';
        }
        $this->doValidate($primary);
        if (!$this->doneValidate()) {
          if ($this->myConfig->get('configuration.allow_reverse')) {
            $this->doValidate($secondary);
          }
          else {
            $this->setError('Unrecognized username or password.');
          }
        }
      }
      // Check for errors.
      if ($this->getError()) {
        $form_state->setErrorByName('name', $this->getError());
      }
    }
  }

  /**
   * Create user. Assumes valid Webtrees user is loaded.
   *
   * @return \Drupal\user\UserInterface
   *   TRUE if initialized but not saved.
   */
  public function createDrupalUser() {
    // Get matching Drupal role.
    if ($this->get('canadmin')) {
      $role = $this->myConfig->get('role.webtrees.administrator');
    }
    else {
      switch ($this->role) {
        case 'admin':
          $role = $this->myConfig->get('role.webtrees.manager');
          break;

        case 'accept':
          $role = $this->myConfig->get('role.webtrees.moderator');
          break;

        case 'edit':
          $role = $this->myConfig->get('role.webtrees.editor');
          break;

        case 'access':
          $role = $this->myConfig->get('role.webtrees.member');
          break;

        default:
          $role = 'authenticated';
          break;
      }
    }
    $values = [
      'name' => $this->userName,
      'mail' => $this->email,
      'status' => 1,
    ];
    $user = \Drupal::entityTypeManager()->getStorage('user')->create($values);
    if ($role != 'authenticated') {
      // Only add non-authenticated roles.
      $user->addRole($role);
    }
    $this->log('Created Drupal user', 'notice');
    return $user;
  }

  /**
   * Validate log in.
   *
   * @param string $type
   *   Should be webtrees or drupal.
   */
  protected function doValidate(string $type) {
    if ($type == 'webtrees') {
      $this->load($this->loginName, 'name_or_email');
      if ($this->isLoaded()) {
        // Check password.
        if ($this->passwordVerify($this->loginPassword)) {
          // Check for email conflicts.
          $user  = user_load_by_name($this->userName);
          $user2 = user_load_by_mail($this->email);

          if ($user) {
            if ($user2 && ($user->get('uid')->value != $user2->get('uid')->value)) {
              // Error: We have two different Drupal users.
              // One matches the username. The other matches the email.
              $this->setError('Webtrees and Drupal user email mismatch. Please notify administraor');
              return;
            }
          }
          else {
            // $user2 may be null but there cannot be an email conflict
            $user = $user2;
          }

          // Check if Webtrees user or matching Drupal user is blocked.
          if (!($this->isBlocked() || ($user && $user->isBlocked()))) {
            // We can log in. Create or update Drupal user.
            if (!$user) {
              // Create Drupal user since there are no conflicts
              // Get matching Drupal role.
              $user = $this->createDrupalUser();
            }
            $user->setPassword($this->loginPassword);
            $user->save();
            $this->loginValid = TRUE;
            return;
          }
          // Present generic error since user is blocked
          // or password does not match.
          $this->setError('Unrecognized username or password.');
        }
      }
    }
    else {
      // Check Drupal login.
      $user  = user_load_by_name($this->loginName);
      $user2 = user_load_by_mail($this->loginName);
      if ($user) {
        if ($user2 && $user->get('uid')->value != $user2->get('uid')->value) {
          $this->setError('Webtrees Drupal error: login name matches two users via email and user name.');
          return;
        }
        // $user and $user2 are the same or no match via email
      }
      else {
        $user = $user2;
      }
      if ($user) {
        if ($user->isBlocked) {
          // done, let Drupal catch block error.
          $this->loginValid = TRUE;
          return;
        }
        $username = $user->get('name')->value;
        if (\Drupal::service('user.auth')->authenticate($username, $this->loginPassword)) {
          // Create or update matching webtrees user.
          $prefix = $this->myConfig->get('database.prefix');

          $this->load($username, 'name');

          if ($this->isLoaded()) {
            if ($this->isBlocked()) {
              // Present generic error since user is blocked.
              $this->setError('Unrecognized username or password.');
              return;
            }
            // Just update user password.
          }
          else {
            // Create user.
            $this->userName = $username;
            $this->realName = $username;
            $this->email = $user->get('mail')->value;
            $this->log(t('Created webtrees user: @name', ['@name' => $username]));

            // Get user's role by matching highest level with Drupal user role.
            $admin = $this->myConfig->get('role.drupal.administrator');
            if ($admin && $user->hasPermission($admin)) {
              $canadmin = '1';
              // Administrator.
              $this->role = 'admin';
            }
            else {
              $canadmin = '0';
              // Default is Member.
              $this->role = 'access';

              $role = $this->myConfig->get('role.drupal.manager');
              if ($role && $user->hasPermission($role)) {
                // Manager.
                $this->role = 'admin';
              }
              else {
                $role = $this->myConfig->get('role.drupal.moderator');
                if ($role && $user->hasPermission($role)) {
                  // Moderator.
                  $this->role = 'accept';
                }
                else {
                  $role = $this->myConfig->get('role.drupal.editor');
                  if ($role && $user->hasPermission($role)) {
                    // Editor.
                    $this->role = 'edit';
                  }
                }
              }
            }

            // May get some settings from configuration form in the future.
            $this->settings = [
              'admin' => '0',
              'canadmin' => $canadmin,
              'autoaccept' => $this->myConfig->get('webtrees_user.autoaccept') ? '1' : '0',
              'comment' => $this->myConfig->get('webtrees_user.comment'),
              'comment_exp' => $this->myConfig->get('webtrees_user.comment_exp'),
              'defaulttab' => '0',
              'language' => $this->myConfig->get('webtrees_user.language'),
              'default_language_filter' => 'en',
              'contactmethod' => $this->myConfig->get('webtrees_user.contactmethod'),
              'max_relation_path' => '1',
              'pwrequested' => '',
              'reg_hascode' => '',
              'reg_timestamp' => time(),
              'session_time' => time(),
              'sync_gedcom' => '0',
              'TIMEZONE' => $this->myConfig->get('webtrees_user.timezone'),

              // At least one of the verified options should be set.
              // Set if both defaults are not set.
              'verified' => $this->myConfig->get('webtrees_user.verified')
                ? '1'
                : ($this->myConfig->get('webtrees_user.verified_by_admin') ? '0' : '1'),
              'verified_by_admin' => $this->myConfig->get('webtrees_user.verified_by_admin') ? '1' : '0',
              'visibleonline' => $this->myConfig->get('webtrees_user.visibleonline') ? '1' : '0',
              'theme' => $this->myConfig->get('webtrees_user.theme'),
            ];
          }
          $this->setPassword($this->loginPassword);
          $this->save();

          $this->loginValid = TRUE;
        }
      }
    }
  }

}
